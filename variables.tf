variable "name" {
  description = "The name of the association"
  type        = string
}

variable "association_id" {
  description = "The ID of the association"
  type        = string
  default     = null
}

variable "document_name" {
  description = "The name of the SSM document"
  type        = string
}

variable "instance_ids" {
  description = "The instance IDs to target"
  type        = list(string)
}

variable "parameters" {
  description = "The parameters for the SSM document"
  type        = map(any)
  default     = {}
}

variable "schedule_expression" {
  description = "The schedule expression"
  type        = string
  default     = null
}

variable "compliance_severity" {
  description = "The compliance severity"
  type        = string
  default     = "UNSPECIFIED"
}

