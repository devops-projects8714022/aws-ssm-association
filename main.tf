resource "aws_ssm_association" "this" {
  name           = var.name
  association_id = var.association_id
  document_name  = var.document_name

  parameters = var.parameters

  targets {
    key    = "InstanceIds"
    values = var.instance_ids
  }

  schedule_expression = var.schedule_expression
  compliance_severity = var.compliance_severity
}

