### Документация

## Модуль Terraform для ассоциации AWS SSM

Этот модуль Terraform создает ассоциацию AWS Systems Manager (SSM) с заданными параметрами. Ассоциация позволяет автоматически запускать команды или сценарии на экземплярах EC2 по расписанию или при наступлении определенных условий.

### Параметры

- `name` (string): Имя ассоциации.
- `association_id` (string, optional): Идентификатор ассоциации (по умолчанию `null`).
- `document_name` (string): Имя SSM-документа.
- `instance_ids` (list(string)): Список идентификаторов экземпляров для целевых машин.
- `parameters` (map(any), optional): Параметры для SSM-документа (по умолчанию `{}`).
- `schedule_expression` (string, optional): Выражение расписания (по умолчанию `null`).
- `compliance_severity` (string, optional): Уровень важности соответствия (по умолчанию `UNSPECIFIED`).

### Пример использования

Пример использования данного модуля в вашем Terraform конфигурационном файле:

```hcl
module "ssm_association" {
  source = "./terraform-aws-ssm-association"

  name                = "MySSMAssociation"
  document_name       = "AWS-RunShellScript"
  instance_ids        = ["i-1234567890abcdef0"]
  parameters          = {
    commands = ["echo Hello, World!"]
  }
  schedule_expression = "rate(1 hour)"
  compliance_severity = "HIGH"
}
```

### Выходные данные (Outputs)

- `ssm_association_id`: Идентификатор SSM-ассоциации.
- `ssm_association_name`: Имя SSM-ассоциации.

### Описание

Этот модуль создаст ассоциацию AWS SSM, которая будет запускать заданную команду или сценарий на указанном экземпляре каждый час. Вы можете настроить параметры модуля в соответствии с вашими требованиями.

---

### Инструкция по установке

1. Создайте папку для модуля, например, `terraform-aws-ssm-association`.
2. Внутри папки создайте файлы `main.tf`, `variables.tf` и `outputs.tf` с соответствующим содержимым.
3. В основной конфигурации Terraform используйте данный модуль, как показано в примере выше.

Этот модуль полезен для автоматизации управления конфигурациями и выполнения сценариев на экземплярах EC2 в AWS, что помогает обеспечить соответствие стандартам безопасности и управляемости.