output "ssm_association_id" {
  description = "The ID of the SSM association"
  value       = aws_ssm_association.this.id
}

output "ssm_association_name" {
  description = "The name of the SSM association"
  value       = aws_ssm_association.this.name
}
